(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : String(i); }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
/**
 * @class
 * @property {string} dbName The db name of the current wiki
 * @property {string} page The name of page
 * @property {string} wikidataUrl
 * @property {string} infoUrl Documentation page for this script
 * @property {Object} api The api object to query data from wikidata
 */
var AutosuggestSitelink = /*#__PURE__*/function () {
  function AutosuggestSitelink() {
    var $link = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    _classCallCheck(this, AutosuggestSitelink);
    this.dbName = mw.config.get('wgDBname');
    this.page = mw.config.get('wgPageName');
    this.title = mw.config.get('wgTitle');
    this.namespace = mw.config.get('wgNamespaceNumber');
    this.wikidataUrl = 'https://www.wikidata.org';
    this.$toolboxLink = $link;

    // When on testwiki, use Test Wikidata.
    if (this.dbName === 'testwiki') {
      this.wikidataUrl = 'https://test.wikidata.org';
    }
    this.api = new mw.ForeignApi(this.wikidataUrl + '/w/api.php');
  }

  /**
   * Check if page has sitelinks related to this wiki,
   * and if it does, suggest possible sitelinks.
   */
  _createClass(AutosuggestSitelink, [{
    key: "checkSitelink",
    value: function checkSitelink() {
      var _this = this;
      this.api.get({
        format: 'json',
        action: 'wbgetentities',
        props: 'sitelinks/urls',
        sitefilter: this.dbName,
        sites: this.dbName,
        titles: this.page
      }).done(function (data) {
        // If "-1" key is not found in entities then the page has sitelinks.
        if (data.entities['-1'] !== undefined) {
          _this.loadTranslations().then(_this.suggestSiteLink.bind(_this));
        } else {
          // Page already linked to item
          // TODO: Prevent this from firing if we've just added a sitelink
          _this.loadTranslations().then(function () {
            _this.$toolboxLink.css('color', '');
            mw.notify(mw.msg('asl-pagelinked'), {
              autoHideSeconds: 'long',
              tag: 'alreadyLinked',
              id: 'asl-alreadylinked'
            });
          });
        }
      });
    }

    /**
     * Check whether a wikidata item is already
     * connected to other sites.
     *
     * @param {string} title
     * @return {jQuery.Promise}
     */
  }, {
    key: "checkPreexistingSitelink",
    value: function checkPreexistingSitelink(title) {
      var _this2 = this;
      var promise = new Promise(function (resolve) {
        _this2.api.get({
          format: 'json',
          action: 'wbgetentities',
          props: 'sitelinks/urls',
          sitefilter: _this2.dbName,
          sites: _this2.dbName,
          ids: title
        }).done(function (data) {
          if (_this2.dbName in data.entities[title].sitelinks) {
            resolve(true);
          }
          resolve(false);
        });
      });
      return promise;
    }

    /**
     * Not found, include link to create new item.
     *
     * @return {jQuery.Object}
     */
  }, {
    key: "itemsNotFound",
    value: function itemsNotFound() {
      var $container = $('<div>');
      $container.append($('<p>').text(mw.msg('asl-notfound')), $('<a>').attr('href', this.wikidataUrl + '/wiki/Special:NewItem?' + new URLSearchParams({
        site: this.dbName,
        page: this.page,
        label: this.title,
        lang: mw.config.get('wgContentLanguage')
      })).text(mw.msg('asl-createnewitem')).attr('target', '_blank').attr('style', 'margin-top: 30px;'));
      return $container;
    }

    /**
     * Find possible related wikidata items based on the page title.
     * If items are found, a dialog is shown with a submission form.
     */
  }, {
    key: "suggestSiteLink",
    value: function suggestSiteLink() {
      var _this3 = this;
      this.api.get({
        format: 'json',
        action: 'query',
        list: 'search',
        srsearch: this.title,
        srprop: 'snippet|titlesnippet'
      }).then(function (data) {
        if (data.query.searchinfo.totalhits === 0) {
          // No sitelinks at all.
          var itemsNotFoundNotification = '.mw-notification-area > #asl-itemsNotFound';
          if ($(itemsNotFoundNotification).length === 0) {
            _this3.$toolboxLink.css('color', '');
            mw.notify(_this3.itemsNotFound(), {
              autoHide: false,
              autoHideSeconds: 'long',
              tag: 'itemsNotFound',
              id: 'asl-itemsNotFound'
            });
          }
          return;
        }
        var Dialog = require('./Dialog.js');
        var items = data.query.search;
        var promises = [];
        items.forEach(function (item) {
          var title = item.title;
          var preexisting = _this3.checkPreexistingSitelink(title);
          promises.push(preexisting);
          preexisting.then(function (result) {
            if (result) {
              item.hasSiteLink = true;
            } else {
              item.hasSiteLink = false;
            }
          });
        });
        Promise.all(promises).then(function () {
          var dialogInstance = new Dialog(_this3, items);
          // Add the dialog to the window manager
          var windowManager = OO.ui.getWindowManager();
          windowManager.addWindows([dialogInstance]);
          windowManager.openWindow(dialogInstance);
          _this3.$toolboxLink.css('color', '');
        });
      });
    }

    /**
     * Send a POST request to add the sitelink.
     *
     * @param {string} item
     * @return {jQuery.Promise}
     */
  }, {
    key: "submit",
    value: function submit(item) {
      return this.api.postWithToken('csrf', {
        action: 'wbsetsitelink',
        id: item,
        linksite: this.dbName,
        linktitle: this.page
      });
    }

    /**
     * Load the translations from the on-wiki messages page.
     *
     * @return {jQuery.Deferred}
     */
  }, {
    key: "loadTranslations",
    value: function loadTranslations() {
      var dfd = $.Deferred(),
        messagesPage = 'MediaWiki:Gadget-AutosuggestSitelink-messages',
        metaApi = new mw.ForeignApi('https://meta.wikimedia.org/w/api.php'),
        userLang = mw.config.get('wgUserLanguage'),
        langPageEn = "".concat(messagesPage, "/en"),
        langPageLocal = "".concat(messagesPage, "/").concat(userLang),
        titles = [langPageEn];
      if (mw.config.get('wgUserLanguage') !== 'en') {
        // Fetch the translation in the user's language, if not English.
        titles.push(langPageLocal);
      }
      var coreMessagesPromise = metaApi.loadMessagesIfMissing(['parentheses-start', 'parentheses-end']);
      var aslMessagesPromise = metaApi.get({
        action: 'query',
        prop: 'revisions',
        titles: titles,
        rvprop: 'content',
        rvslots: 'main',
        format: 'json',
        formatversion: 2
      }).then(function (resp) {
        var messagesLocal = {},
          messagesEn = {};
        resp.query.pages.forEach(function (page) {
          var parsedContent;
          if (page.missing) {
            return;
          }

          /**
           * The content model of the messages page is wikitext so that it can be used with
           * Extension:Translate. Consequently, it's easy to break things, so we do a
           * try/catch, try some commons fixes, and indicate the likely culprit to the user.
           */
          var content = page.revisions[0].slots.main.content;
          try {
            try {
              parsedContent = JSON.parse(content);
            } catch (_unused) {
              // A common failure reason is because of the HTML that's added for
              // untranslated messages, so remove this and try to parse again.
              // eslint-disable-next-line es-x/no-string-prototype-replaceall
              content = content.replaceAll('<span lang="en" dir="ltr" class="mw-content-ltr">', '');
              // eslint-disable-next-line es-x/no-string-prototype-replaceall
              content = content.replaceAll('</span>', '');
              parsedContent = JSON.parse(content);
            }
          } catch (_unused2) {
            // If it's still failing, there's something else wrong with the
            // messages (e.g. a double quote within a translated message).
            var metaLink = '<a href="https://meta.wikimedia.org/wiki/">' + page.title + '</a>';
            return OO.ui.alert($('<span>Unable to parse the messages page ' + metaLink + '. ' + 'There may have been a recent change that contains invalid JSON.</span>'), {
              title: 'Something went wrong'
            });
          }
          if (page.title === langPageLocal) {
            messagesLocal = parsedContent.messages;
          } else {
            messagesEn = parsedContent.messages;
          }
        });
        mw.messages.set(Object.assign({}, messagesEn, messagesLocal));
      });
      Promise.all([coreMessagesPromise, aslMessagesPromise]).then(function () {
        dfd.resolve();
      });
      return dfd;
    }
  }]);
  return AutosuggestSitelink;
}();
function init() {
  var $link = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  $link.css({
    color: '#C4C2C1'
  });
  Promise.all([
  // Resource loader modules
  mw.loader.using(['mediawiki.action.view.postEdit', 'mediawiki.ForeignApi', 'mediawiki.Title', 'oojs-ui-core', 'oojs-ui-windows'])]).then(function () {
    var autosuggestSitelink = new AutosuggestSitelink($link);
    autosuggestSitelink.checkSitelink();
  });
}
$.when(mw.loader.using(['mediawiki.util']), $.ready).then(function () {
  // Debug flag: bypass other checks and launch the dialog.
  if (window.AutosuggestSitelinkDebug) {
    init();
    return;
  }

  // Return when not in a content namespace or the article does not exist.
  var nsIds = mw.config.get('wgNamespaceIds');
  var validNamespaces = mw.config.get('wgContentNamespaces')
  // Remove some namespaces that should never have sitelinks.
  .filter(function (e) {
    return e !== nsIds.file && e !== nsIds.page && e !== nsIds.index;
  });
  // Add Category NS.
  validNamespaces.push(nsIds.category);
  var ns = mw.config.get('wgNamespaceNumber');
  if (!validNamespaces.includes(ns) || mw.config.get('wgArticleId') === 0) {
    return;
  }

  // Add postEdit hook.
  mw.hook('postEdit').add(init);

  // Add sidebar link.
  var $link = $(mw.util.addPortletLink('p-tb', '#', 'AutosuggestSiteLink', 'tb-autosuggestsitelink', null, null, '#t-whatlinkshere').querySelector('a'));
  $link.on('click', function (e) {
    init($link);
    e.preventDefault();
  });
});

},{"./Dialog.js":2}],2:[function(require,module,exports){
"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _callSuper(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _isNativeReflectConstruct() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct = function _isNativeReflectConstruct() { return !!t; })(); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _classPrivateMethodInitSpec(obj, privateSet) { _checkPrivateRedeclaration(obj, privateSet); privateSet.add(obj); }
function _checkPrivateRedeclaration(obj, privateCollection) { if (privateCollection.has(obj)) { throw new TypeError("Cannot initialize the same private elements twice on an object"); } }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : String(i); }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function _assertClassBrand(e, t, n) { if ("function" == typeof e ? e === t : e.has(t)) return arguments.length < 3 ? t : n; throw new TypeError("Private element is not present on this object"); }
var _Dialog_brand = /*#__PURE__*/new WeakSet();
/**
 * @class
 * @property {AutosuggestSitelink} aslContext
 * @property {Array<Object>} items
 */
var Dialog = /*#__PURE__*/function (_OO$ui$ProcessDialog) {
  _inherits(Dialog, _OO$ui$ProcessDialog);
  /**
   * @param {AutosuggestSitelink} aslContext
   * @param {Array<Object>} items
   * @constructor
   */
  function Dialog(aslContext, _items) {
    var _this;
    _classCallCheck(this, Dialog);
    _this = _callSuper(this, Dialog);
    /**
     * Get the fieldset of items with radio buttons.
     *
     * @param {Array<Object>} items
     * @return {OO.ui.FieldsetLayout}
     */
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _Dialog_brand);
    _this.aslContext = aslContext;
    _this.items = _items;
    _this.helpUrl = 'https://meta.wikimedia.org/wiki/Meta:AutosuggestSitelink';

    // These properties are set here instead of using the static keyword above
    //   because mw.msg() isn't available in the static context.
    Dialog.title = mw.msg('asl-popuptitle');
    Dialog.actions = [{
      action: 'submit',
      label: mw.msg('asl-submit'),
      flags: ['primary', 'progressive'],
      disabled: true
    }, {
      action: 'help',
      label: mw.msg('asl-help'),
      icon: 'helpNotice',
      href: _this.helpUrl
    }, {
      action: 'close',
      flags: ['safe', 'close']
    }];

    // Add properties needed by the ES5-based OOUI inheritance mechanism.
    // This roughly simulates OO.inheritClass()
    Dialog.parent = Dialog["super"] = OO.ui.ProcessDialog;
    OO.initClass(OO.ui.ProcessDialog);
    Dialog["static"] = Object.create(OO.ui.ProcessDialog["static"]);
    Object.keys(Dialog).forEach(function (key) {
      Dialog["static"][key] = Dialog[key];
    });
    return _this;
  }

  /**
   * @param {...*} args
   * @override
   */
  _createClass(Dialog, [{
    key: "initialize",
    value: function initialize() {
      var _get2;
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      (_get2 = _get(_getPrototypeOf(Dialog.prototype), "initialize", this)).call.apply(_get2, [this].concat(args));
      this.itemsFieldset = _assertClassBrand(_Dialog_brand, this, _getItemsFieldset).call(this, this.items);
      this.panel = new OO.ui.PanelLayout({
        padded: true,
        expanded: false
      });
      this.panel.$element.append(this.itemsFieldset.$element);
      this.$body.append(this.panel.$element);
    }
  }, {
    key: "getActionProcess",
    value:
    /**
     * @param {string} action
     * @return {OO.ui.Process}
     * @override
     */
    function getActionProcess(action) {
      var _this2 = this;
      return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", this).call(this, action).next(function () {
        if (action === 'submit') {
          return _this2.aslContext.submit(_this2.itemSelect.findSelectedItem().data);
        }
        if (action === 'help') {
          window.open(_this2.helpUrl);
        }
        return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", _this2).call(_this2, action);
      }).next(function () {
        if (action === 'submit') {
          _this2.close({
            action: action
          });
          // eslint-disable-next-line max-len
          // Show a message and reload the page so that any dependencies (including AutosuggestSitelink)
          // are updated with the new sitelink.
          mw.notify(mw.msg('asl-itemlinked'), {
            autoHideSeconds: 'long'
          });
          window.location.reload();
        }
        return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", _this2).call(_this2, action);
      }).next(function () {
        if (action === 'close') {
          _this2.close({
            action: action
          });
        }
        return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", _this2).call(_this2, action);
      });
    }
  }]);
  return Dialog;
}(OO.ui.ProcessDialog);
function _getItemsFieldset(items) {
  var _this3 = this;
  this.itemSelect = new OO.ui.RadioSelectWidget().connect(this, {
    choose: function choose() {
      return _this3.getActions().setAbilities({
        submit: true
      });
    }
  });

  /**
   * Quick helper function to strip out HTML from a string,
   * which is present in the search result snippets because
   * HTML is used to highlight the searched term.
   *
   * @param {string} str
   * @return {string}
   */
  var stripHTML = function stripHTML(str) {
    return str.replace(/<\/?.+?>/ig, '');
  };
  items.forEach(function (item) {
    var $qItemLink = $('<a>').attr('href', _this3.aslContext.wikidataUrl + '/wiki/' + item.title).attr('target', '_blank').text(item.title);
    var $qItemLabel = $('<span>').text(mw.msg('parentheses-start') + stripHTML(item.titlesnippet) + mw.msg('parentheses-end'));
    var $qItemDesc = $('<span>').attr('style', 'font-style: italic; display: block; padding-right: 5px;').text(stripHTML(item.snippet) || mw.msg('asl-nodescription'));
    // Check if the item has a sitelink
    if (item.hasSiteLink) {
      var $qItemWarning = $('<span>').text(mw.msg('asl-alreadylinked')).attr('style', 'font-weight: bold; display: block;');
      $qItemDesc.append($qItemWarning);
    }
    _this3.itemSelect.addItems([new OO.ui.RadioOptionWidget({
      label: $('<span>').append($qItemLink, ' ', $qItemLabel, $qItemDesc),
      data: item.title
    })]);
  });
  var description = new OO.ui.Element({
    $content: $('<p>').text(mw.msg('asl-heading-desc'))
  });
  return new OO.ui.FieldsetLayout({
    label: mw.msg('asl-heading'),
    items: [description, this.itemSelect]
  });
}
_defineProperty(Dialog, "name", 'aslDialog');
_defineProperty(Dialog, "size", 'large');
module.exports = Dialog;

},{}]},{},[1]);
